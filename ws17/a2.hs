data Rope
  = Leaf String
  | Inner Rope Int Rope deriving (Show)

example :: Rope
example =
  Inner (Leaf "Hello") 5(Inner (Leaf ", w") 3 (Leaf "orld!"))

ropeLength :: Rope -> Int
ropeLength (Leaf s) = length s
ropeLength (Inner _ w r) = w + (ropeLength r)

ropeConcat :: Rope -> Rope -> Rope
ropeConcat l r =
  Inner l (ropeLength l) r

ropeSplitAt :: Int -> Rope -> (Rope, Rope)
ropeSplitAt i (Leaf str) =
  (Leaf $ take i str, Leaf $ drop i str)
ropeSplitAt i (Inner l w r) =
  if i == w then
    (l, r)
  else if i < w then
    let (l', r') = ropeSplitAt i l in
    (l', ropeConcat r' r)
  else
    let (l', r') = ropeSplitAt (i - w) r in
    (ropeConcat l l', r')

ropeSplitAt' :: Int -> Rope -> (Rope, Rope)
ropeSplitAt' i (Leaf str) =
  (Leaf $ take i str, Leaf $ drop i str)
ropeSplitAt' i (Inner left weight right)
  | i == weight = (left, right)
  | i < weight  = let (l1, l2) = ropeSplitAt i left in (l1, ropeConcat l2 right)
  | i > weight  = let (r1, r2) = ropeSplitAt (i - weight) right in (ropeConcat left r1, r2)
