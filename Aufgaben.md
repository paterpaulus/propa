# ProPa-Aufgaben nach Thema

Enthält alle Klausuren der ProPa-Website https://pp.ipd.kit.edu/lehre/WS201819/paradigmen/.

## Haskell

- [x] SS14/Aufgabe 1 (11 P.): Listen
- [x] SS14/Aufgabe 2 (18 P.): Lexalische Analyse, DEA
- [x] WS14/Aufgabe 1 (12 P.): Newton-Iteration
- [x] WS14/Aufgabe 2 (12 P.): Binärbäume
- [ ] SS15/Aufgabe 1 (25 P.): Stringverarbeitung
- [ ] WS15/Aufgabe 1 (20 P.): Unendliche Listen
- [x] WS15/Aufgabe 2 (5 P.): Typklassen
- [ ] SS16/Aufgabe 1 (26 P.): Mehrwegbäume
- [ ] SS17/Aufgabe 1 (22 P.): Semimagische Quadrate
- [ ] SS17/Aufgabe 2 (7 P.): Bäume
- [ ] WS17/Aufgabe 1 (7 P.): Divide-and-Conquer-Floatsummierung
- [x] WS17/Aufgabe 2 (18 P.): Ropes
- [x] SS18/Aufgabe 1 (15 P.): Vollkommene Zahlen
- [x] SS18/Aufgabe 2 (15 P.): `ListBuilder`

## λ-Kalkül

- [x] SS14/Aufgabe 3 (15 P.): Natürliche Zahlen, β-Reduktion
- [ ] SS14/Aufgabe 4 (16 P.): Natürliche Zahlen, Typherleitung
- [x] WS14/Aufgabe 3 (17 P.): Freie Variablen, Redex, Normalform
- [x] SS15/Aufgabe 2 (15 P.): SKI-Kalkül
- [x] WS15/Aufgabe 4 (17 P.): Paare, Euklids Algorithmus
- [x] SS16/Aufgabe 4 (16 P.): Zählerobjekte, β-Reduktion
- [ ] SS17/Aufgabe 6 (15 P.): Gleichheit auf Church-Zahlen
      Noch nicht fertig, b) und c) fehlen.
- [x] WS17/Aufgabe 5 (12 P.): Alternativer Fixpunktkombinator
- [x] SS18/Aufgabe 4 (13 P.): Currying

## Prolog

- [x] SS14/Aufgabe 5 (20 P.): Java-Bytecode, Codeerzeugung
- [x] WS14/Aufgabe 4 (12 P.): Grundlagen, λ-Kalkül
- [x] SS15/Aufgabe 4 (17 P.): Haus vom Nikolaus (Eulerpfade)
- [x] WS15/Aufgabe 3 (20 P.): Rucksack-Problem
- [x] SS16/Aufgabe 2 (9 P.): NEA
- [x] SS16/Aufgabe 3 (8 P.): Grundlagen
- [ ] SS17/Aufgabe 3 (7 P.): Prädikatenlogik (freie Variablen)
- [x] SS17/Aufgabe 4 (12 P.): Unifikationsalgorithmus nach Robinson
- [x] WS17/Aufgabe 3 (18 P.): Gewichtete Bäume
- [x] SS18/Aufgabe 3 (20 P.): Rot-Schwarz-Bäume

## MPI

- [x] SS14/Aufgabe 8 (11,5 P.): Matrizenmultiplikation
- [x] WS14/Aufgabe 9 (9 P.): Send/Recv
- [x] SS15/Aufgabe 5 (15 P.): Kollektive Operationen
- [ ] WS15/Aufgabe 6 (5 P.): Kollektive Operationen
- [x] SS16/Aufgabe 8 (7 P.): MPI-Funktionen kennen
- [x] SS17/Aufgabe 7 (8 P.): MPI-Funktionen kennen
- [ ] WS17/Aufgabe 6 (7 P.): Skalarprodukt
- [ ] SS18/Aufgabe 6 (10 P.): Matrizen-/Vektormultiplikation

## Java-Bytecode

- [x] SS16/Aufgabe 9 (10 P.): `if`, arithmetische/logische Operationen
- [x] SS17/Aufgabe 12 (5 P.): Java-Programm rekonstruieren

## C

- [ ] SS14/Aufgabe 9 (5,5 P.): Precedence-Regel (Typen lesen)

## Compiler

- [x] SS14/Aufgabe 10 (10 P.): First-/Follow-Mengen
- [x] WS14/Aufgabe 10 (22 P.): Dangling-Else-Problem
- [ ] SS15/Aufgabe 8 (18 P.): LISP
- [ ] WS15/Aufgabe 10 (10 P.): X10-Teilmenge
- [x] SS17/Aufgabe 11 (3 P.): First-/Follow-Mengen
- [x] WS17/Aufgabe 9 (20 P.): Linksfaktorisierung, Rekursiver Abstieg
- [ ] SS18/Aufgabe 8 (12 P.): Rekursiver Abstieg

## Typsysteme

- [x] WS14/Aufgabe 5 (15 P.): Allgemeinster Unifikator, Herleitungsbaum
- [x] SS15/Aufgabe 3 (15 P.): Allgemeinster Unifikator, Herleitungsbaum
- [x] WS15/Aufgabe 5 (18 P.): Allgemeinster Unifikator, Herleitungsbaum
- [x] SS16/Aufgabe 5 (21 P.): Allgemeinster Unifikator, Herleitungsbaum
- [x] SS17/Aufgabe 5 (19 P.): Herleitungsbaum
- [x] WS17/Aufgabe 4 (15 P.): Typisierung von λ-Termen, Herleitungsbaum
- [x] SS18/Aufgabe 5 (15 P.): Allgemeinster Unifikator, Herleitungsbaum

## Design by Contract

- [x] SS17/Aufgabe 10 (6 P.): Informelles DbC
- [x] WS17/Aufgabe 8 (8 P.): Konto mit JML
- [x] SS18/Aufgabe 7ef (5 P.): Gefilterte Summe mit JML

## Parallelität

- [x] WS14/Aufgabe 6 (12 P.): Wissensfragen zu Parallelität
- [x] WS14/Aufgabe 7 (5 P.): Speedup-Berechnung
- [ ] WS15/Aufgabe 7 (12 P.): Synchronisation in Java
- [ ] WS15/Aufgabe 8 (7 P.): Semaphore in Java
- [ ] SS16/Aufgabe 6 (16 P.): `BufferedAsyncWriter` in Java
- [x] SS16/Aufgabe 7 (7 P.): Thread-safe `ArrayList` in Java
- [ ] WS17/Aufgabe 7 (15 P.): Paralleler Filter
- [ ] SS18/Aufgabe 7abcd (15 P.): Gefilterte Summe mit JML

### Nicht mehr relevant

- [ ] SS17/Aufgabe 8 (8 P.): Aktoren in Java (Akka)
- [ ] SS17/Aufgabe 9 (8 P.): Petri-Netze in Java

### Nicht mehr relevant

- [ ] WS15/Aufgabe 9 (6 P.): Parallelisierung in X10

## Flynn's Taxonomie

### Nicht mehr relevant

- [ ] SS14/Aufgabe 6 (4 P.): Tabelle ausfüllen

## Scala

### Nicht mehr relevant

- [ ] SS14/Aufgabe 7 (9 P.): Dining Philosophers Problem
- [ ] WS14/Aufgabe 8 (4 P.): Scala-Code-Lückentext
- [ ] SS15/Aufgabe 6 (7 P.): Verständnis
- [ ] SS15/Aufgabe 7 (8 P.): Aktoren
